import React, { Component } from 'react'

class FormattedPrice extends Component {
    render() {
        return <div>{`R$ ${Number(this.props.price).toFixed(2).replace('.', ',')}`}</div>
    }
}

export default FormattedPrice